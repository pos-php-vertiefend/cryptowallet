const app = Vue.createApp({

    data() {
        return {
            prices:[],
            aktuelleprices:[],
            wallets: [],
            purchases: []
        }
    },
    mounted() {
        // auto call methods after app started
        this.loadPrices()
        this.loadWallets()
        this.loadPurchases()
        this.loadCurrentPrices()
    },
    methods: {
        loadPrices(){
           axios.get("https://api.bitpanda.com/v1/ticker").then(
               (response) => {
                    //prices = [key/btc value/euro]
                    let prices =[];
                    let keys = Object.keys(response.data);
                    let values = Object.values(response.data);
                    let europrices = [];
                    values.forEach( value => {
                            europrices.push(value.EUR)
                        }
                    )
                    let obj = {};
                    for(let i = 0; i < keys.length; i++){
                        let test = {currencies: keys[i], values: europrices[i]};
                        //let test = obj[keys[i]] = europrices[i];
                        prices.push(test);
                    }
                    //prices = Object.entries(obj);


                    this.prices = prices;
                   //console.log(prices);
            })
        },
        loadCurrentPrices(){
            axios.get("https://api.bitpanda.com/v1/ticker").then(
                response => {
                    let pricess = [];
                    for (const [currency, value] of Object.entries(response.data)) {
                        pricess[currency] = parseFloat(value.EUR)
                    }
                    console.log(pricess)
                    this.aktuelleprices = pricess;
                    console.log(this.aktuelleprices)
                })
        },
        loadWallets(){
            this.wallets = [];
            axios.get("server/api/wallet").then
                (response =>{
                    this.wallets = response.data
                    //console.log("Wallets"+this.wallets);
                }

            )
            return this.wallets;
        },

        loadPurchases(){
            axios.get("http://localhost/cryptowallet/server/api/purchase").then
            (response =>{
                    this.purchases = response.data
                }

            )
        },

        loadPurchasesOfWallet(walletId){
            this.purchases = [];
            axios.get("http://localhost/cryptowallet/server/api/wallet/"+walletId+"/purchase").then
            (response =>{
                    this.purchases = response.data
                }

            )
            return this.purchases;
        },

        filterPurchase(){
            let WalletArray = [];
            WalletArray = this.loadWallets();
            WalletArray.forEach(wallet=>{
                this.loadPurchasesOfWallet(wallet.id);

                }
             )
        },

        addPurchase(purchase){
            axios.post("http://localhost/cryptowallet/server/api/purchase", purchase)
                .then(response => a.loadWallets());
            //console.log("Es funktioniert in der Main");
            //console.log(purchase);
        }

    }
})
