app.component('purchase-form', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        wallets: {
            type: Array,
            required: true
        }
    },
    template:
    /*html*/`        
    
    <div class="wallet-container" >
       <form class="wallet-display" @submit.prevent="onSubmit">
        <h3>Cryptowährung kaufen</h3>
        <div>
        <label for="name">Cryptowährung:</label>
            <div>
                <select v-model="selectedCrypto">
                    <option v-for="price in prices" v-bind:value="{currency: price.currencies, value: price.values}">
                        {{price.currencies}}  {{ price.values}}€
                    </option>
                </select>
               
            </div>
    </div><label for="name">Wallet:</label>
           <div>
               <select v-model="selectedWallet">
                   <option v-for="wallet in wallets" v-bind:value="{id: wallet.id, name: wallet.name}">
                       {{wallet.name}}
                   </option>
               </select>
           </div>
    <label for="review">Menge:</label>      
    <input id="inputMenge" v-model.number="inputMenge">
           
    <label for="rating">Wert:</label>
    <input class="button" type="submit" value="Kaufen">  
  </form>
</div>
`,
    data() {
        return {
            selectedCrypto: "",
            selectedWallet: "",
            inputMenge: ""

        }
    },
    methods: {
        onSubmit(){
                let purchase = {
                    date: new Date().toISOString().slice(0, 19).replace('T', ' '),
                    amount: this.inputMenge,
                    price: this.selectedCrypto.value,
                    wallet_id: this.selectedWallet.id
                }
                this.$emit("purchaseAdded", purchase);
                console.log(purchase);
        }
    },
    computed: {

    }
})
