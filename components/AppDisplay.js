app.component('app-display', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        wallets: {
            type: Array,
            required: true
        },
        purchases: {
            type: Array,
            required: true
        },
        aktuelleprices: {
            type: Array,
            required: true
        }
    },
    template:
    /*html*/
        `
       
        <div class="wallet-container">
            <purchase-form @purchaseAdded="addPurchaseMain" v-bind:prices="prices" v-bind:wallets="wallets"></purchase-form>
            <div class="wallet-display"></div>
            <wallet-list v-bind:purchases="purchases" v-bind:prices="prices" v-bind:wallets="wallets" v-bind:aktuelleprices="aktuelleprices"></wallet-list>
        </div>
        
   `,
    data() {
        return {

        }
    },
    methods: {
        addPurchaseMain(purchase){
            //console.log("Es funktioniert");
            this.$emit("gagga", purchase)
            //console.log(purchase);
        }
    }
})
