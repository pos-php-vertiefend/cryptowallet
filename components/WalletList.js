app.component('wallet-list', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        wallets: {
            type: Array,
            required: true
        },
        purchases: {
            type: Array,
            required: true
        },
        aktuelleprices: {
            type: Array,
            required: true
        }
    },
    template:
    /*html*/
        `
        <div class="wallet-container" >
            <form class="wallet-display">
                <h3>Wallets: {{gesamtPreis}}€</h3>
                <div>
                    <ul>
                        <li v-for="wallet in wallets"> {{ wallet.amount }} {{wallet.name}} : {{wallet.currency}} {{ currentValue(wallet) }} {{ win(wallet) }}</li>
                    </ul>
                </div>
            </form>
        </div>
    `,
    data() {
        return {
            purchasesOfWallet: [],
            purchaseArray: [],

        }
    },
    methods: {
            pricesLoaded(){
                return Object.keys(this.aktuelleprices).length >0;
            },
            currentValue(wallet){
                if(this.pricesLoaded() && wallet.amount > 0){
                    return (this.aktuelleprices[wallet.currency] * wallet.amount).toFixed(2) + " €";
                }
                else {
                    return "";
                }
            },
        win(wallet){
            if(this.pricesLoaded() && wallet.amount > 0){
                const currentV = this.aktuelleprices[wallet.currency] * wallet.amount;
                return '(' + ((currentV / wallet.price - 1) * 100).toFixed(1) + '%)';
            }
            else{
                return "";
            }
        }


    },

    computed: {
        gesamtPreis(){
            let gesamtpreis = 0;
            this.wallets.forEach(wallet =>{
                gesamtpreis += wallet.amount*this.aktuelleprices[wallet.currency];
            })
            return (gesamtpreis.toFixed(2));

        }
    }
})
