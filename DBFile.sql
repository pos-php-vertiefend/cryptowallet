CREATE database php43;
USE php43;

# TODO
# purchase (id, name, altitude, location)
CREATE TABLE `purchase`
(
    `id`          int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `date`        DATETIME,
    `amount`       double,
    `price`       double,
    `wallet_id` int(11) NOT NULL
);


CREATE TABLE `wallet`
(
    `id`          int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name`    VARCHAR(255),
    `currency`    VARCHAR(255),
    `amount`    double,
    `price`       double
);


ALTER TABLE `purchase`
    ADD CONSTRAINT `fk_wallet_id` FOREIGN KEY (`wallet_id`) REFERENCES `wallet` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;