<?php

require_once('RESTController.php');
require_once('models/Wallet.php');
require_once('models/Purchase.php');

class WalletRESTController extends RESTController
{
    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }

    /**
     * get single/all purchase or search purchase
     * all purchase: GET api.php?r=wallet
     * single purchase: GET api.php?r=wallet/25 -> args[0] = 25
     * all purchases group by currency: GET api.php?r=wallet/ -> verb = currency, args[0] = BTC
     */
    private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            $model = Wallet::get($this->args[0]);  // single wallet
            $this->response($model);
        }
        else if ($this->verb == null &&  sizeof($this->args) > 1) {
            $model = Purchase::getAllByWallet($this->args[0]);             // all purchase by walleties
            $this->response($model);
        }
        else if ($this->verb == null && empty($this->args)) {
            $model = Wallet::getAll();             // all wallets
            $this->response($model);
        } else {
            $this->response("Bad request Michael hat das Internet gelöscht", 400);
        }
    }

    /**
     * create purchase: POST api.php?r=wallet
     */
    private function handlePOSTRequest()
    {
        $model = new Wallet();
        $model->setName($this->getDataOrNull('name'));
        $model->setAmount($this->getDataOrNull('amount'));
        $model->setPrice($this->getDataOrNull('price'));
        $model->setCurrency($this->getDataOrNull('currency'));

        if ($model->save()) {
            $this->response("OK", 201);
        } else {
            $this->response($model->getErrors(), 400);
        }
    }

    /**
     * update purchase: PUT api.php?r=wallet/25 -> args[0] = 25
     */
    private function handlePUTRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {

            $model = new Wallet();
            $model->setName($this->getDataOrNull('name'));
            $model->setAmount($this->getDataOrNull('amount'));
            $model->setPrice($this->getDataOrNull('price'));
            $model->setCurrency($this->getDataOrNull('currency'));

            if ($model->save()) {
                $this->response("OK");
            } else {
                $this->response($model->getErrors(), 400);
            }

        } else {
            $this->response("Not Found", 404);
        }
    }

    /**
     * delete purchase: DELETE api.php?r=purchase/25 -> args[0] = 25
     */
    private function handleDELETERequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            Wallet::delete($this->args[0]);
            $this->response("OK", 200);
        } else {
            $this->response("Not Found", 404);
        }
    }

}
