<?php

require_once 'DatabaseObject.php';

class Wallet implements DatabaseObject, JsonSerializable
{
    private $id;
    private $name;
    private $currency;
    private $amount;
    private $price;

    private $errors = [];

    public function validate()
    {
        return $this->validateName() & $this->validateCurrency();
    }

    /**
     * create or update an object
     * @return boolean true on success
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }

    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO wallet (name, currency, amount, price) values(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->currency, $this->amount, $this->price));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * Saves the object to the database
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE wallet set name = ?, amount = ?, price = ?, currency = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->amount, $this->price, $this->currency, $this->id));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql2 = "SELECT w.*, sum(p.amount) AS amount, sum(p.amount + price) AS price FROM `wallet` w LEFT JOIN purchase p ON w.id = p.wallet_id GROUP BY w.id";
        $stmt = $db->prepare($sql2);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Wallet');  // ORM
        Database::disconnect();
        return $item !== false ? $item : null;
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {
        $db = Database::connect();
        $sql = "SELECT w.*, sum(p.amount) AS amount, sum(p.amount + price) AS price FROM `wallet` w LEFT JOIN purchase p ON w.id = p.wallet_id GROUP BY w.id";
        $stmt = $db->prepare($sql);
        $stmt->execute();

        // fetch all datasets (rows), convert to array of Purchase-objects (ORM)
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Wallet');

        Database::disconnect();

        return $items;
    }



    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM wallet WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }



    private function validateName() {
        if ($this->name == '') {
            $this->errors['name'] = "Name darf nicht leer sein";
            return false;
        } else if (strlen($this->name) > 64) {
            $this->errors['name'] = "Name zu lang";
            return false;
        } else {
            unset($this->errors['name']);
            return true;
        }
    }

    private function validateCurrency() {
        if (strlen($this->currency) == 0) {
            $this->errors['currency'] = "Waehrung ungueltig";
            return false;
        } else if (strlen($this->currency) > 32) {
            $this->errors['currency'] = "Waehrung zu lang (max. 32 Zeichen)";
            return false;
        } else {
            unset($this->errors['currency']);
            return true;
        }
    }

    /**
     * define attributes which are part of the json output
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            "id" => intval($this->id),
            "name" => $this->name,
            "currency" => $this->currency,
            "amount" => doubleval($this->amount),
            "price" => doubleval($this->price),
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

}
